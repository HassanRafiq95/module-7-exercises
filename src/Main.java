public class Main {

    public static void main(String[] args) {
        Horse horse = new Horse("Jerry");
        horse.describe();
        Budgie budgie = new Budgie("Donald");
        budgie.describe();
        Snake snake = new Snake("Slippery");
        snake.describe();
    }
}
