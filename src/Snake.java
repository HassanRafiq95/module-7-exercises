public class Snake extends Animal{

    String name;

    public Snake(String name) {
        super(name, "Snake", 0);
        this.name = name;
    }
}
