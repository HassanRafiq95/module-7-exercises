public class Horse extends Animal {

    String name;

    public Horse(String name) {
        super(name, "horse", 4);
        this.name = name;
    }
}
