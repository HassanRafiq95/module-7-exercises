import java.util.ArrayList;

public class AnimalListExercise {

    public static void main(String[] args) {

        ArrayList<Animal> animalArrayList = new ArrayList<>();
        animalArrayList.add(new Horse("Horsey Horse"));
        animalArrayList.add(new Budgie("Budget Budgie"));
        animalArrayList.add(new Snake("Snakey Snake"));

        for (Animal animal : animalArrayList) {
            animal.describe();
        }
    }
}