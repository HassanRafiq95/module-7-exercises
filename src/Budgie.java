public class Budgie extends Animal {

    String name;

    public Budgie(String name) {
        super(name,"Budgie", 2);
        this.name = name;
    }
}
