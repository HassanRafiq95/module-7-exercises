public class Animal {

    protected String name;
    protected String species;
    protected int numLegs;

    public Animal(String name, String species, int numLegs)
    {
        this.name = name;
        this.species = species;
        this.numLegs = numLegs;
    }

    public void describe()
    {
        if (this.numLegs == 0)
        {
            System.out.println("My name is " + this.name + ", I am a " + this.species + " and I have no legs");

        }
        else

        System.out.println("My name is " + this.name + ", I am a " + this.species + " and I have " + this.numLegs + " legs");
    }

}
